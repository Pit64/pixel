"Pixel" theme updated to use new Recalbox 9.2+ syntax - 2024-02-13 by Pit64.

If you have some assets to add for the missing systems or any configuration for crt, you can still do a merge request and it will be reviewed.

## Original readme file

Theme 'pixel' "Recalbox Conversion" -  by Rookervik / RockAddicted
For use with EmulationStation (http://www.emulationstation.org/)

Screenshots
===========

Startup Screen

![Startup](https://gitlab.com/recalbox/recalbox-themes/raw/master/resources/pixel/screenshots/startup.png)

Detail View with scraped info

![Detail](https://gitlab.com/recalbox/recalbox-themes/raw/master/resources/pixel/screenshots/detail.png)

Changelog
=========

## Theme 'pixel' v1.8 "Recalbox Conversion" - 11-08-2017 by Rookervik / RockAddicted

### Updates:
- November, 09th, 2017: Added missing Recalbox systems:
  - Amiga600
  - Amiga1200
  - Cave Story
  - Favorites
  - FBA_Libretro
  - Lutro
  - Moonlight
  - MSX1
  - MSX2
  - PCEngine CD
  - PrBoom
  - Supergrafx
  - ZX 81

## Theme 'pixel' v1.7 - 05-29-2016 by Rookervik

Thanks to muriani for the super-sexy Sega CD logo!
Thanks to Omnija for helping me fix some code errors.

### Updates:
- May 29th, 2016: Added AGS, Steam, and Desktop systems.
- April 3rd, 2016: Added support for Pipplware.

License
=======

See LICENSE.

LOGO NOTICE:

The used logos and trademarks are copyright of their respective owners.